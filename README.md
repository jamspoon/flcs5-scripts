# Flash CS5 Scripts

A collection of extensions and scripts written for Flash Professional CS5 that make my work easier.

## Scripts
+ **jamsprite.jsfl**: The JAM Sprite Exporter.
    - I primarily use Flash for video game animation work. When using Flash for working out basic character animation, I like to organize each action into its own Scene. Unfortunately, Flash's default PNG sequence exporting makes this all a waking nightmare, with no organization or differentiation between one animation and another. Exporting with this script instead, each frame is exported into a separate `FILENAME_out` folder, and each animation is clearly distinguished from the others, as every frame is formatted as `ANIMATIONNAME__FRAMENUMBER.PNG`.
    - **NOTES**:
        - This script can and will export duplicate PNGs for frames with a duration longer than 1. Remedying this would require analyzing each layer in the timeline to ensure that every layer at that frame shared a non-one duration and overlapped such that the next frame could be avoided. This obviously isn't *impossible*, but it's a case of diminishing returns - you may as well just delete the duplicate PNG (it'll probably take less time).
