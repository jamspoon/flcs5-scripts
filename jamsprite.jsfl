/**
 *  
 *  SPRITE EXPORTER
 *  I like using Flash CS5 for prototyping and roughing out temporary video game sprite animations.
 *  It's great for that purpose, but it's not super great at exporting these sprites neatly. There's no
 *  built-in organization of the filenames, so animations are all lumped together as one of hundreds of
 *  anim_XXXX.png files. Using scenes for different animations doesn't help, because that's not what scenes
 *  are designed for in Flash CS5 (go figure).
 * 
 *  So that's why I wrote this script. With each separate animation contained in a separate Flash scene,
 *  we can now export the project and name our exported PNGs accordingly - i.e. WALK__001.png or JUMP__003.png.
 *  Finally, order comes to the wastelands!
 * 
 *      - jamspoon
 * 
 */

document.save();

var scenes=fl.getDocumentDOM().timelines;
var scene_number=0;

// Sprites are currently exported to a directory located next to your .FLA file named [filename]_out/ (i.e., in the same folder as "myanim.fla", sprites will be found in the folder "myanim_out").
var output_uri=String(document.pathURI);
output_uri=output_uri.substr(0,output_uri.lastIndexOf("/"));
output_uri=output_uri.concat("/",document.name.split(".")[0],"_out/");

FLfile.createFolder(output_uri);

while (scene_number<scenes.length)
{
    document.currentTimeline=scene_number;

    var scene_current=scenes[scene_number];
    var scene_name=scene_current.name;
    var scene_frame_count=scene_current.frameCount;

    for (var scene_frame_current=0;scene_frame_current<scene_frame_count;scene_frame_current++)
    {
        var frame_filename=scene_name+"__"+("0000"+scene_frame_current).slice(-4)+".png";

        fl.getDocumentDOM().getTimeline().currentFrame=scene_frame_current;
        document.exportPNG(output_uri.concat(frame_filename),true,true);
    }

    scene_number++;
}